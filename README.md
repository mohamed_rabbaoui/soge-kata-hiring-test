# SoGe Account Management project

## Brief Intro :
To test different endpoints, you can access directly to this swagger link :
http://localhost:8080/swagger-ui.html#/

## Requirements :
To get a functionnal project, you need to get :
- A pre-created and running mysql database.

**Batch configuration :**

- `spring.datasource.url` : url to the pre-created mysql database
- `spring.datasource.username` : pre-created mysql database username
- `spring.datasource.password`: pre-created mysql database password
