package sgcib.banking.managment.exception;

public class NotAvailableAmountException extends RuntimeException {

  public NotAvailableAmountException(String msg) {
    super(msg);
  }
}
