package sgcib.banking.managment.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

@Getter
@RequiredArgsConstructor
public class GlobalException extends Exception {
  private final HttpStatus status;

  GlobalException(String message, HttpStatus status) {
    super(message);
    this.status = status;
  }
}
