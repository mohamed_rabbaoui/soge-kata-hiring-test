package sgcib.banking.managment.exception;

public class AccountDoNotExistsException extends RuntimeException {

  public AccountDoNotExistsException(String msg) {
    super(msg);
  }
}
