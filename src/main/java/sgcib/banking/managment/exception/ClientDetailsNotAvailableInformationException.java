package sgcib.banking.managment.exception;

public class ClientDetailsNotAvailableInformationException extends RuntimeException {

  public ClientDetailsNotAvailableInformationException(String msg) {
    super(msg);
  }
}
