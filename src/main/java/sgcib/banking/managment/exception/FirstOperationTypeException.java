package sgcib.banking.managment.exception;

public class FirstOperationTypeException extends RuntimeException {

  public FirstOperationTypeException(String msg) {
    super(msg);
  }
}
