package sgcib.banking.managment.exception;

public class ClientAlreadyExistsException extends RuntimeException {

  public ClientAlreadyExistsException(String msg) {
    super(msg);
  }
}
