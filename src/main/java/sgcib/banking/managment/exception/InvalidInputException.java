package sgcib.banking.managment.exception;

import org.springframework.http.HttpStatus;

public class InvalidInputException extends GlobalException {
  public InvalidInputException(String message) {
    super(message, HttpStatus.UNPROCESSABLE_ENTITY);
  }
}
