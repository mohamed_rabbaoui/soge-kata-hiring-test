package sgcib.banking.managment.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import sgcib.banking.managment.model.Account;

public interface AccountRepository extends JpaRepository<Account, String> {
  Optional<Account> findByAccountId(String accountId);
}
