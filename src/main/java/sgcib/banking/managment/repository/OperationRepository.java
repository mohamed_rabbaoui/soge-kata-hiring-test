package sgcib.banking.managment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sgcib.banking.managment.model.Operation;

public interface OperationRepository extends JpaRepository<Operation, String> {}
