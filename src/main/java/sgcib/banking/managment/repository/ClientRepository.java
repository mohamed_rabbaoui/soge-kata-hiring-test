package sgcib.banking.managment.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import sgcib.banking.managment.model.Client;

public interface ClientRepository extends JpaRepository<Client, String> {
  public Optional<Client> findByClientId(String clientId);
}
