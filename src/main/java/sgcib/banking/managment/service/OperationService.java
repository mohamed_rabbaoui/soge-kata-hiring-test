package sgcib.banking.managment.service;

import sgcib.banking.managment.model.Operation;

public interface OperationService {

  void saveNewOperation(Operation operation);
}
