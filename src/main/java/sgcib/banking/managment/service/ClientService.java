package sgcib.banking.managment.service;

import sgcib.banking.managment.dto.ClientDetailsDTO;
import sgcib.banking.managment.exception.ClientAlreadyExistsException;
import sgcib.banking.managment.model.Client;

public interface ClientService {
  Client addNewClient(ClientDetailsDTO client) throws ClientAlreadyExistsException;

  Client findClientByIdOrDefine(ClientDetailsDTO clientDetails);
}
