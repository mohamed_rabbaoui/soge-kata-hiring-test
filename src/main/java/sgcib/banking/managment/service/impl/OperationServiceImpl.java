package sgcib.banking.managment.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sgcib.banking.managment.model.Operation;
import sgcib.banking.managment.repository.OperationRepository;
import sgcib.banking.managment.service.OperationService;

@Service
public class OperationServiceImpl implements OperationService {

  @Autowired OperationRepository operationRepository;

  @Override
  public void saveNewOperation(Operation operation) {
    operationRepository.save(operation);
  }
}
