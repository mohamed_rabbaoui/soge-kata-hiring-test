package sgcib.banking.managment.service.impl;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sgcib.banking.managment.dto.ClientDetailsDTO;
import sgcib.banking.managment.exception.ClientAlreadyExistsException;
import sgcib.banking.managment.model.Client;
import sgcib.banking.managment.repository.ClientRepository;
import sgcib.banking.managment.service.ClientService;

@Service
public class ClientServiceImpl implements ClientService {

  @Autowired ClientRepository clientRepository;

  @Override
  public Client addNewClient(ClientDetailsDTO client) throws ClientAlreadyExistsException {
    Client clientToSave =
        Client.builder()
            .clientId(client.getClientId())
            .firstName(client.getFirstName())
            .lastName(client.getLastName())
            .profession(client.getProfession())
            .address(client.getAddress())
            .build();
    return clientRepository.save(clientToSave);
  }

  @Override
  public Client findClientByIdOrDefine(ClientDetailsDTO clientDetails) {
    Optional<Client> client = clientRepository.findByClientId(clientDetails.getClientId());
    return client.orElseGet(() -> addNewClient(clientDetails));
  }
}
