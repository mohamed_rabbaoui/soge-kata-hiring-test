package sgcib.banking.managment.service.impl;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sgcib.banking.managment.dto.AccountDTO;
import sgcib.banking.managment.dto.OperationDTO;
import sgcib.banking.managment.dto.OperationHistoryDTO;
import sgcib.banking.managment.exception.AccountDoNotExistsException;
import sgcib.banking.managment.exception.ClientDetailsNotAvailableInformationException;
import sgcib.banking.managment.exception.FirstOperationTypeException;
import sgcib.banking.managment.exception.NotAvailableAmountException;
import sgcib.banking.managment.model.Account;
import sgcib.banking.managment.model.Client;
import sgcib.banking.managment.model.Operation;
import sgcib.banking.managment.model.OperationTypeEnum;
import sgcib.banking.managment.repository.AccountRepository;
import sgcib.banking.managment.repository.ClientRepository;
import sgcib.banking.managment.service.AccountManagementService;
import sgcib.banking.managment.service.ClientService;
import sgcib.banking.managment.service.OperationService;
import sgcib.banking.managment.utils.ConstantUtils;

@Service
public class AccountManagementServiceImpl implements AccountManagementService {

  @Autowired ClientRepository clientRepository;

  @Autowired ClientService clientService;

  @Autowired AccountRepository accountRepository;

  @Autowired OperationService operationService;

  @Override
  public String addNewAccount(AccountDTO accountDTO)
      throws ClientDetailsNotAvailableInformationException {
    if (accountDTO.getClientDetails() == null
        || accountDTO.getClientDetails().getClientId() == null) {
      throw new ClientDetailsNotAvailableInformationException(
          "Client details should at least contains at least client id to add a new account");
    }
    if (!OperationTypeEnum.getFromValue(accountDTO.getOperation().getOperationType())
        .equals(ConstantUtils.DEPOSIT_TYPE_VALUE)) {
      throw new FirstOperationTypeException(
          "First operation when creation account should be deposit");
    }
    String clientId = accountDTO.getClientDetails().getClientId();
    Client client = clientService.findClientByIdOrDefine(accountDTO.getClientDetails());
    Operation operation =
        accountDTO.getOperation().mapToOperation(accountDTO.getOperation().getAmount());
    operationService.saveNewOperation(operation);
    Account savedAccount =
        accountRepository.save(
            Account.builder()
                .accountId(clientId.concat(String.valueOf(System.currentTimeMillis())))
                .client(client)
                .operationList(Collections.singletonList(operation))
                .build());
    return savedAccount.getAccountId();
  }

  @Override
  public OperationHistoryDTO executeOperation(OperationDTO operationDTO) {
    Optional<Account> account = accountRepository.findByAccountId(operationDTO.getAccountId());
    if (!account.isPresent()) {
      throw new AccountDoNotExistsException(
          MessageFormat.format(
              "Account with id {0} does not exists in the database.", operationDTO.getAccountId()));
    }
    List<Operation> operations =
        account.get().getOperationList().stream()
            .sorted(Comparator.comparing(Operation::getOperationDate).reversed())
            .collect(Collectors.toList());
    Double amount =
        OperationTypeEnum.getFromValue(operationDTO.getOperationType())
                .equals(ConstantUtils.DEPOSIT_TYPE_VALUE)
            ? operationDTO.getAmount()
            : operationDTO.getAmount() * (-1);
    if (operations.get(0).getBalance() + amount < 0) {
      throw new NotAvailableAmountException(
          MessageFormat.format(
              "Withdrawal operation with amount {0} can not be executed. "
                  + "Your actual balance is : {1}",
              operationDTO.getAmount(), operations.get(0).getBalance()));
    }
    Operation operation = operationDTO.mapToOperation(operations.get(0).getBalance() + amount);
    operationService.saveNewOperation(operation);
    operations.add(operation);
    accountRepository.save(
        Account.builder()
            .accountId(operationDTO.getAccountId())
            .client(account.get().getClient())
            .operationList(operations)
            .build());
    return operation.mapToOperationHistory();
  }

  @Override
  public List<OperationHistoryDTO> getOperationsHistoryByAccountId(String accountId) {
    Optional<Account> account = accountRepository.findByAccountId(accountId);
    if (!account.isPresent()) {
      throw new AccountDoNotExistsException(
          MessageFormat.format("Account with id {0} does not exists in the database.", accountId));
    }
    return account.get().getOperationList().stream()
        .map(Operation::mapToOperationHistory)
        .collect(Collectors.toList());
  }
}
