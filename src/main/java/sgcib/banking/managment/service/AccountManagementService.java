package sgcib.banking.managment.service;

import java.util.List;
import sgcib.banking.managment.dto.AccountDTO;
import sgcib.banking.managment.dto.OperationDTO;
import sgcib.banking.managment.dto.OperationHistoryDTO;
import sgcib.banking.managment.exception.ClientDetailsNotAvailableInformationException;

public interface AccountManagementService {
  String addNewAccount(AccountDTO accountDTO) throws ClientDetailsNotAvailableInformationException;

  OperationHistoryDTO executeOperation(OperationDTO operationDTO);

  List<OperationHistoryDTO> getOperationsHistoryByAccountId(String accountId);
}
