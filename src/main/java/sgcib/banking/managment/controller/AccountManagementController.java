package sgcib.banking.managment.controller;

import javax.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import sgcib.banking.managment.dto.AccountDTO;
import sgcib.banking.managment.dto.OperationDTO;
import sgcib.banking.managment.service.AccountManagementService;
import sgcib.banking.managment.utils.FunctionUtils;

@RestController
@Slf4j
public class AccountManagementController {

  @Autowired private AccountManagementService accountManagementService;

  @PostMapping("/admin/add-new-account")
  public ResponseEntity addNewAccount(@RequestBody @NotNull AccountDTO accountDTO) {
    try {
      FunctionUtils.validateInput(accountDTO);
      return new ResponseEntity<>(
          accountManagementService.addNewAccount(accountDTO), HttpStatus.OK);
    } catch (Exception e) {
      log.error(e.getMessage());
      return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  @PostMapping("/admin/execute-operation")
  public ResponseEntity executeOperation(@RequestBody @NotNull OperationDTO operationDTO) {
    try {
      FunctionUtils.validateInput(operationDTO);
      return new ResponseEntity<>(
          accountManagementService.executeOperation(operationDTO), HttpStatus.OK);
    } catch (Exception e) {
      log.error(e.getMessage());
      return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  @PostMapping("/get-operations-history-by-account-id")
  public ResponseEntity getOperationsListHistoryByAccountId(
      @RequestBody @NotNull String accountId) {
    try {
      return new ResponseEntity<>(
          accountManagementService.getOperationsHistoryByAccountId(accountId), HttpStatus.OK);
    } catch (Exception e) {
      log.error(e.getMessage());
      return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
}
