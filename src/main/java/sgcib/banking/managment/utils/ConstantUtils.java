package sgcib.banking.managment.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ConstantUtils {
  public static final String DEPOSIT_TYPE_VALUE = "DEPOSIT";
  public static final String WITHDRAWAL_TYPE_VALUE = "WITHDRAWAL";
}
