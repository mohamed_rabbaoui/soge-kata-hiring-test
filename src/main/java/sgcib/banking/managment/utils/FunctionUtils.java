package sgcib.banking.managment.utils;

import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import sgcib.banking.managment.exception.InvalidInputException;

public interface FunctionUtils {

  static <T> void validateInput(T input) throws InvalidInputException {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    Validator validator = factory.getValidator();
    Set<ConstraintViolation<T>> violations = validator.validate(input);
    if (!violations.isEmpty()) {
      String exceptionMessage =
          violations.stream()
              .map(ConstraintViolation::getMessage)
              .collect(Collectors.joining(" & "));
      throw new InvalidInputException(exceptionMessage);
    }
  }
}
