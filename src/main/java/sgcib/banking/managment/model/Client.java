package sgcib.banking.managment.model;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "client")
@Builder
public class Client {

  @Id
  @Column(name = "client_id")
  private String clientId;

  @Column(name = "firstname")
  private String firstName;

  @Column(name = "lastname")
  private String lastName;

  @Column(name = "address")
  private String address;

  @Column(name = "profession")
  private String profession;

  @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
  private List<Account> accountList;
}
