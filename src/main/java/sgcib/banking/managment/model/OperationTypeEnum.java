package sgcib.banking.managment.model;

public enum OperationTypeEnum {
  DEPOSIT("DEPOSIT"),
  WITHDRAWAL("WITHDRAWAL");

  public final String value;

  OperationTypeEnum(String value) {
    this.value = value;
  }

  public static String getFromValue(OperationTypeEnum value) {
    if (value == OperationTypeEnum.WITHDRAWAL) {
      return "WITHDRAWAL";
    }
    return "DEPOSIT";
  }
}
