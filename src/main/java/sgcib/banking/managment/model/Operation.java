package sgcib.banking.managment.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sgcib.banking.managment.dto.OperationHistoryDTO;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "operation")
@Builder
public class Operation {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "operation_id")
  private long operationId;

  @Column(name = "operation_type")
  private String operationType;

  @Column(name = "amount")
  private Double amount;

  @Column(name = "operation_date")
  private Long operationDate;

  @Column(name = "balance")
  private Double balance;

  @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
  private Account account;

  public OperationHistoryDTO mapToOperationHistory() {
    return OperationHistoryDTO.builder()
        .amount(amount)
        .balance(balance)
        .operationDate(operationDate)
        .operationType(operationType)
        .build();
  }
}
