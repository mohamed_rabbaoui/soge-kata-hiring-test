package sgcib.banking.managment.dto;

import lombok.Data;

@Data
public class ClientDetailsDTO {
  private String clientId;
  private String firstName;
  private String lastName;
  private String address;
  private String profession;
}
