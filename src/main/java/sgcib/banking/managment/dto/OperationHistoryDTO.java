package sgcib.banking.managment.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OperationHistoryDTO {
  private String operationType;
  private Double amount;
  private Long operationDate;
  private Double balance;
}
