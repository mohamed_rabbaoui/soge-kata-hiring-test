package sgcib.banking.managment.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.Builder;
import lombok.Data;
import sgcib.banking.managment.model.Operation;
import sgcib.banking.managment.model.OperationTypeEnum;

@Data
@Builder
public class OperationDTO {
  @NotNull(message = "Operation type should not be null")
  private OperationTypeEnum operationType;

  @NotNull(message = "amount should not be null")
  @Positive(message = "operation amount should be positive")
  private Double amount;

  private String accountId;

  public Operation mapToOperation(Double balance) {
    return Operation.builder()
        .operationType(operationType.toString())
        .amount(amount)
        .operationDate(System.currentTimeMillis())
        .balance(balance)
        .build();
  }
}
