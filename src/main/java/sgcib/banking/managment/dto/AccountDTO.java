package sgcib.banking.managment.dto;

import lombok.Data;

@Data
public class AccountDTO {
  private ClientDetailsDTO clientDetails;
  private OperationDTO operation;
}
